##  Aplicación para compilar En Desktop Con IONIC  ##

```bash 
#para correr el repositorio por primera vez descargar las dependencias con : 
npm install
# Si se crea un proyecto de este tipo por primera vez correr
npm install -g electron --unsafe-perm=true --allow-root
#para verificar versión
electron --version
# despues instalar 
npm install -g electron-packager --unsafe-perm=true --allow-root
#para verificar version 
electron-packager --version 
#para instalar el instalador para mac 
npm install electron-installer-dmg 
#instalador parawindows 
npm install electron-installer-windows 
#instalar dependencias en el proyecto 
npm install --save-dev electron 
npm install --save-dev electron-packager
npm install --save-dev electron-installer-dmg
npm install --save-dev electron-installer-windows 
```

# En packaje.json hay que agregar estos scripts  #
```json 

  "scripts": {
    "ng": "ng",
    "start": "ng serve",
    "build": "ng build",
    "prod": "ng build -prod",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e",
    "electron": "electron .",
    "electron-serve": "ng build --prod && electron .",
    "mac-release": "electron-packager . --overwrite --platform=darwin --arch=x64 --icon=src/assets/icon.icns --prune=true --out=release-builds",
     "windows-release": "electron-packager . electron-app --overwrite --asar=true --platform=win32 --arch=ia32 --icon=build/icon.ico --prune=true --out=release-builds --version-string.CompanyName=CE --version-string.FileDescription=CE --version-string.ProductName='electron-app'",
    "linux-release": "electron-packager . electron-tutorial-app --overwrite --asar=true --platform=linux --arch=x64 --icon=src/assets/icon.png --prune=true --out=release-builds",
    "mac-build": "electron-installer-dmg ./release-builds/ionic-desktop-darwin-x64/ionic-desktop.app builds/ionic-desktop",
    "windows-build": "node src/windows-build.js"
  },

```
#Previamente a realizar npm run windows-release hay que hacer una compilación con npm run electron-serve para que genere la carpeta www#


# script windows-buid.json para compilar una aplicación como instalador a partir del output que nos da el comando npm run windows-release este se ejecuta con npm run windows-build busca en la carpeta release-build el ejecutable para empaquetarlo #

```jsavascript

const createWindowsInstaller = require('electron-winstaller').createWindowsInstaller
const path = require('path')

getInstallerConfig()
  .then(createWindowsInstaller)
  .catch((error) => {
    console.error(error.message || error)
    process.exit(1)
  })

function getInstallerConfig () {
  const rootPath = path.join('./')
  const outPath = path.join(rootPath, 'release-builds')

  return Promise.resolve({
    appDirectory: path.join(outPath, 'electron-app-win32-ia32/'),
    authors: 'Jacob MCfly',
    noMsi: true,
    outputDirectory: path.join(outPath, 'windows-installer'),
    exe: 'electron-app.exe',
    setupExe: 'electron-app-installer.exe',
    setupIcon: path.join(rootPath, 'src/assets','icon.ico') // dirección del icono 
  })
}

```


#Notas#

Existe un bug cuando quieres recargar la pagina en modo desarrollo hau que cambiar en el index.html a vacio de esta manera

```html

<base href="" />

```