import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterGraficatorPage } from './filter-graficator.page';

describe('FilterGraficatorPage', () => {
  let component: FilterGraficatorPage;
  let fixture: ComponentFixture<FilterGraficatorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterGraficatorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterGraficatorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
