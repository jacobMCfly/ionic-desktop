import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FilterGraficatorPage } from './filter-graficator.page';

const routes: Routes = [
  {
    path: '',
    component: FilterGraficatorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FilterGraficatorPage]
})
export class FilterGraficatorPageModule {}
