import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  pages = [ 
    { title: 'home', url: '/menu/submenu/home' ,icon:'' },
    { title: 'dashboard', url: '/menu/submenu/dashboard',icon:''  },
    { title: 'configuration', url: '/menu/submenu/configuration',icon:''  },
    { title: 'filter-graficator', url: '/menu/submenu/filter-graficator',icon:''  },
    { title: 'users', url: '/menu/submenu/users',icon:''  },
    { title: 'reports', url: '/menu/submenu/reports' ,icon:'' },
  ];
   selectedPath = this.router.url; 
//  selectedPath ='';
  
  constructor(private router: Router) {     
    console.log(this.selectedPath);
    this.router.events.subscribe((event:RouterEvent)=>{ 
          this.selectedPath = event.url;
         
      })

   }

  ngOnInit() {
  }

}
