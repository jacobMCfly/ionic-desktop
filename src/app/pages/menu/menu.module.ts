import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: 'submenu',
    component: MenuPage,
    children: [
      { path: 'home',loadChildren: '../../home/home.module#HomePageModule'},
      { path: 'dashboard',loadChildren: '../dashboard/dashboard.module#DashboardPageModule'},
      { path: 'configuration', loadChildren: '../configuration/configuration.module#ConfigurationPageModule' },
      { path: 'filter-graficator', loadChildren: '../filter-graficator/filter-graficator.module#FilterGraficatorPageModule' },
      { path: 'users', loadChildren: '../users/users.module#UsersPageModule' },
      { path: 'reports', loadChildren: '../reports/reports.module#ReportsPageModule' },
    ]
  },
  {
    path:'',
    redirectTo:'submenu/home'
  } 
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
